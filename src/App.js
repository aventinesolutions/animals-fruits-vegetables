// Part 2

import React, { Component } from 'react'
import './App.css'
import NaturalEntities from './components/NaturalEntities'

class App extends Component {
  render() {
    return (
      <div className="App">
        <NaturalEntities/>
      </div>
    )
  }
}

export default App
